# maxx-fs

MaXX File Service (MaXX FS) is a High Performance and multi-threaded file-system service optimized for multi-core systems that supports CPU core affinity and partitioning for maximum IO performance and efficiency.